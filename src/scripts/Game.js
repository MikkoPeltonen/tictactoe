import EventManager from "./EventManager";
import Board from "./Board";
import Grid from "./Grid";


export default class Game {
  constructor(container, size) {
    this.moveCompleted = this.moveCompleted.bind(this);
    this.restart = this.restart.bind(this);

    this.setup(container, size);
  }

  setup(container, size) {
    this.grid = new Grid(size);
    this.board = new Board(container, this, this.grid);

    this.eventManager = new EventManager();
    this.eventManager.on("moveCompleted", this.moveCompleted);
    this.eventManager.on("restart", this.restart);

    this.board.setEventManager(this.eventManager);
  }

  start() {
    this.nextMove();
  }

  restart() {
    this.grid.clear();
    this.board.restart();
    this.setFirstPlayer();
    this.start();
  }

  nextMove() {
    this.players[this.currentPlayer].requestMove();
  }

  moveCompleted(params) {
    this.grid.makeMove(this.currentPlayer, params.move);
    this.board.updateBoard();

    let [winner, streak] = this.grid.checkBoard();
    if (winner !== false) {
      this.board.showEndScreen(this.players[winner].name);
    } else if (this.grid.movesAvailable()) {
      this.switchPlayers();
      this.nextMove();
    } else {
      this.board.showEndScreen(null);
    }
  }

  setPlayers(player1, player2) {
    this.players = [player1, player2];

    this.players.forEach(player => {
      player.setEventManager(this.eventManager);
      player.setGrid(this.grid);
    });

    this.setFirstPlayer();
  }

  setFirstPlayer() {
    this.currentPlayer = Math.round(Math.random());

    if (this.players[this.currentPlayer].constructor.name === "Human") {
      this.board.enableBindings();
    }
  }

  switchPlayers() {
    this.currentPlayer = this.currentPlayer ? 0 : 1;

    if (this.players[this.currentPlayer].constructor.name === "Human") {
      this.board.enableBindings();
    } else {
      this.board.disableBindings();
    }
  }
}
