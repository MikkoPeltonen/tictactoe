import Game from "./Game";
import Computer from "./Computer";
import Human from "./Human";


$(() => {
  let game = new Game("#game", 3),
      human = new Human("John"),
      computer = new Computer("AI-1", 1, 200),
      computer2 = new Computer("AI-2", 2, 200);

  game.setPlayers(human, computer2);
  game.start();
});
