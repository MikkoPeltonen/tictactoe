import Player from "./Player";
import { randomInt } from "./helpers";


export default class Computer extends Player {
  constructor(name, difficulty, thinkingTime = 2000) {
    super();

    this.name = name;
    this.difficulty = difficulty;
    this.thinkingTime = thinkingTime;
  }

  requestMove() {
    setTimeout(() => {
      this.eventManager.emit("moveCompleted", {
        move: this.grid.getRandomAvailableCell(),
      });
    }, randomInt(Math.floor(0.75 * this.thinkingTime), Math.floor(1.25 * this.thinkingTime)));
  }
}
