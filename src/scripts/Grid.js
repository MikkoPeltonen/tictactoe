import { onChange } from "./helpers";


export default class Grid {
  constructor(size) {
    this.size = size;
    this.gridWatchers = [];

    this.generateGrid = this.generateGrid.bind(this);
    this.clear = this.clear.bind(this);
    this.onChange = this.onChange.bind(this);
    this.gridChanged = this.gridChanged.bind(this);
    this.getAvailableCells = this.getAvailableCells.bind(this);
    this.getRandomAvailableCell = this.getRandomAvailableCell.bind(this);
    this.makeMove = this.makeMove.bind(this);

    this.generateGrid();
  }

  generateGrid() {
    this.cells = [];
    for (let row = 0; row < this.size; row++) {
      let gridRow = this.cells[row] = [];
      for (let col = 0; col < this.size; col++) {
        gridRow[col] = null;
      }
    }

    this.cells = onChange(this.cells, this.gridChanged);
  }

  clear() {
    this.generateGrid();
  }

  onChange(callback) {
    this.gridWatchers.push(callback);
  }

  gridChanged() {
    this.gridWatchers.forEach(callback => {
      callback();
    });
  }

  getAvailableCells() {
    let cells = [];
    for (let y = 0; y < this.size; y++) {
      for (let x = 0; x < this.size; x++) {
        if (this.cells[y][x] === null) {
          cells.push({ x, y });
        }
      }
    }

    return cells;
  }

  isCellAvailable(cell) {
    return this.cells[cell.y][cell.x] === null;
  }

  movesAvailable() {
    return !!this.getAvailableCells().length;
  }

  getRandomAvailableCell() {
    let cells = this.getAvailableCells();
    return cells[Math.floor(Math.random() * cells.length)];
  }

  checkBoard() {
    // Check rows
    for (let row = 0; row < this.size; row++) {
      if (this.cells[row].every(cell => cell === this.cells[row][0]) && this.cells[row][0] !== null)
        return [this.cells[row][0], [{ x: 0, y: row }, { x: this.size - 1, y: row }]];
    }

    // Check columns
    for (let col = 0; col < this.size; col++) {
      if (this.cells.every(row => row[col] === this.cells[0][col]) && this.cells[0][col] !== null)
        return [this.cells[0][col], [{ x: col, y: 0 }, { x: col, y: this.size - 1 }]];
    }

    // Check diagonals, first from top-left to bottom-right, and then top-right to bottom-left
    let diagonal1 = this.cells.map((row, pos) => row[pos]),
        diagonal2 = this.cells.map((row, pos) => row[this.size - pos - 1]);

    if (diagonal1.every(cell => cell === diagonal1[0]) && diagonal1[0] !== null)
      return [diagonal1[0], [{ x: 0, y: 0 }, { x: this.size - 1, y: this.size - 1 }]];

    if (diagonal2.every(cell => cell === diagonal2[0]) && diagonal2[0] !== null)
      return [diagonal2[0], [{ x: this.size - 1, y: 0 }, { x: 0, y: this.size - 1 }]];

    return [false, null];
  }

  makeMove(player, move) {
    this.cells[move.y][move.x] = player;
  }
}
