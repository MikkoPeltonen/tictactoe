import "../styles/game.scss";


export default class Board {
  constructor(container, game, grid) {
    this.$container = $(container);
    this.game = game;
    this.grid = grid;
    this.bindingsEnabled = false;

    this.generateLayout();
    this.bindMouse();
  }

  setEventManager(eventManager) {
    this.eventManager = eventManager;
  }

  generateLayout() {
    let html = `<div class="game"><div class="grid">`;
    for (let row = 0; row < this.grid.size; row++) {
      html += `<div class="row">`;
      for (let col = 0; col < this.grid.size; col++) {
        html += `<div class="cell cell-${col}-${row}" data-x="${col}" data-y="${row}"></div>`;
      }
      html += `</div>`;
    }
    html += `</div>`;
    html += `<div class="end-screen"><h3></h3><span class="restart">New Game</span></div>`;
    html += `</div>`;

    this.$container.html(html);

    this.$endScreen = this.$container.find(".end-screen").first();
    this.$endScreenTitle = this.$endScreen.find("h3").first();
  }

  updateBoard() {
    for (let y = 0; y < this.grid.size; y++) {
      for (let x = 0; x < this.grid.size; x++) {
        let cell = this.grid.cells[y][x];
        let $cell = this.$container.find(`.cell-${x}-${y}`).first();

        if (cell === 0) {
          $cell.addClass("cross");
        } else if (cell === 1) {
          $cell.addClass("circle");
        } else {
          $cell.removeClass("cross circle");
        }
      }
    }
  }

  bindMouse() {
    let self = this;
    let $cells = this.$container.find(".cell");

    $cells.hover(function mouseEnter() {
      if (!self.bindingsEnabled)
        return;

      let $cell = $(this);

      if (self.game.currentPlayer === 0) {
        $cell.addClass("hover hover-cross");
      } else {
        $cell.addClass("hover hover-circle");
      }
    }, function mouseLeave() {
      if (!self.bindingsEnabled)
        return;

      let $cell = $(this);
      $cell.removeClass("hover hover-cross hover-circle");
    });

    $cells.click(function clickCell() {
      if (!self.bindingsEnabled)
        return;

      let $cell = $(this);
      let x = parseInt($cell.attr("data-x"));
      let y = parseInt($cell.attr("data-y"));

      if (self.grid.isCellAvailable({ x, y }))
        self.handleClick({ x, y });
    });

    this.$container.on("click", ".restart", () => {
      this.eventManager.emit("restart", {});
    });
  }

  disableBindings() {
    this.bindingsEnabled = false;
  }

  enableBindings() {
    this.bindingsEnabled = true;
  }

  handleClick(position) {
    this.eventManager.emit("cellClicked", {
      position,
      player: 0,
    });
  }

  showEndScreen(name) {
    this.$endScreen.css("visibility", "visible");

    if (name !== null) {
      this.$endScreenTitle.html(`${name} won!`);
    } else {
      this.$endScreenTitle.html("It's a draw!");
    }
  }

  restart() {
    this.$endScreen.css("visibility", "hidden");
    this.updateBoard();
  }
}
