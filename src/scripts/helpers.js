export function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export function onChange(object, callback) {
  const handler = {
    get(target, property, receiver) {
      const desc = Object.getOwnPropertyDescriptor(target, property);
      const value = Reflect.get(target, property, receiver);

      if (desc && !desc.writable && !desc.configurable)
        return value;

      try {
        return new Proxy(target[property], handler);
      } catch (error) {
        return value;
      }
    },

    defineProperty(target, property, descriptor) {
      callback();
      return Reflect.defineProperty(target, property, descriptor);
    },
  };

  return new Proxy(object, handler);
}
