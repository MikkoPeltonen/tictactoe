import Player from "./Player";


export default class Human extends Player {
  constructor(name) {
    super();

    this.name = name;
  }

  setEventManager(eventManager) {
    this.eventManager = eventManager;

    this.eventManager.on("cellClicked", (context) => {
      if (context.player === 0) {
        this.makeMove(context.position);
      }
    });
  }

  makeMove(position) {
    this.eventManager.emit("moveCompleted", {
      move: position,
    });
  }
}