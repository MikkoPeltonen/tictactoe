export default class EventManager {
  constructor() {
    this.events = {};
  }

  on(type, callback, context) {
    (this.events[type] = this.events[type] || []).push([callback, context]);
  }

  emit(type) {
    (this.events[type] || []).forEach(([callback, context]) => {
      callback.apply(context, Array.prototype.slice.call(arguments, 1));
    });
  }
}
